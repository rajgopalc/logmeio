
var fs = require('fs'),
	uuid = require('node-uuid'),
	bodyParser = require('body-parser'),
	faye = require('faye'),
	http = require('http'),
	express = require('express');


var app = express(),
	server = http.createServer(app);

var bayeux = new faye.NodeAdapter({mount: '/faye', timeout: 45});

var addedClients = [];

bayeux.attach(server);

setTimeout(function(){ 
            addedClients = [];
    }, 100000);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
//parse json
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

// set the view engine to ejs
app.set('view engine', 'ejs');

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
 });

app.get('/', function(request,response){
    response.render('welcomepage',{data : addedClients});
});

app.get('/:uuid',function(request,response){
		response.render('logMe',{uuid : request.params.uuid});
});

app.post('/listen',function(request,response){
	response.send(uuid.v1());
});



//The below method is to be called by our script where console.log is overwritten.
app.post('/message',function(request,response,next){
	//var JSONdata = request.params.Data;
	//var parsed_JSON = JSON.parse(JSONdata);
    
	console.log(request.body.UUID, request.body.Data);
    if(addedClients.indexOf(request.body.UUID) == -1){
        addedClients.push(request.body.UUID);
    }
	//here pub sub comes in - coz I can't redirect or send a response back to the client sending message - I can only send an empty response
	bayeux.getClient().publish('/'+request.body.UUID, {'Data': request.body.Data, 'UUID':request.body.UUID});
	response.send('');
});

server.listen(9099, function(){
	console.log("Listening on port 9099");
});